A basic docker image for CI testing of AXS-Labs.

The image is based on ubuntu:16.04 and further includes:

- docker
- docker-compose
